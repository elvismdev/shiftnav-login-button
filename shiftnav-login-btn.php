<?php
/**
 * Plugin Name: ShiftNav Login Button
 * Plugin URI: https://bitbucket.org/grantcardone/shiftnav-login-button
 * Description: Adds login/logout button on ShiftNav mobile menu bar. 
 * Version: 1.0.0
 * Author: Elvis Morales
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.2.4
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

add_action( 'shiftnav_toggle_after_content', 'slb_add_login' );
function slb_add_login() {
	if ( is_user_logged_in() ) { 
		echo '<div class="shiftnav-login-btn">' . wp_loginout( get_site_url(), false ) . '</div>'; 
	} else {
		echo '<div class="shiftnav-login-btn"><a data-toggle="modal" class="btn" data-target="#myModal2">Login</a></div>';
	}
}

add_filter( 'loginout', 'slb_loginout_btn_class' );
function slb_loginout_btn_class( $text ) {
	$selector = 'class="btn"';
	$text = str_replace( '<a ', '<a '.$selector, $text );
	return $text;
}